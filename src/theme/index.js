import * as $C from "../constants/colors";

const breakpoints = ["320px", "640px", "1024px", "1400px"];

// aliases
breakpoints.sm = breakpoints[0];
breakpoints.md = breakpoints[1];
breakpoints.lg = breakpoints[2];
breakpoints.xl = breakpoints[3];

const space = [0, 4, 8, 16, 24, 32, 40, 48, 56, 64, 128, 160, 256, 512];
const fontSizes = [12, 14, 16, 18, 20, 24, 32, 42];

const colors = {
  GRAY_DARK: $C.GRAY_DARK,
  GRAY_MEDIUM: $C.GRAY_MEDIUM,
  GRAY_LIGHT: $C.GRAY_LIGHT,
  GREEN_LIGHT: $C.GREEN_LIGHT,
  ORANGE: $C.ORANGE,
  ORANGE_LIGHT: $C.ORANGE_LIGHT,
  TRANSPARENT: $C.TRANSPARENT,
  WHITE: $C.WHITE
};

const fontFamilies = {
  roboto: '"Roboto", sans-serif'
};

const textStyles = {
  spanText: {
    as: "span",
    fontSize: [1],
    fontWeight: "normal",
    lineHeight: ["18px"],
    fontFamily: fontFamilies.roboto
  },
  labelText: {
    as: "label",
    fontSize: [0],
    fontWeight: "normal",
    lineHeight: ["14px"],
    fontFamily: fontFamilies.roboto
  }
};

const sizes = ["400px", "720px", "1124px", "1480px"];

export default {
  breakpoints,
  colors,
  fontSizes,
  space,
  textStyles,
  sizes
};
