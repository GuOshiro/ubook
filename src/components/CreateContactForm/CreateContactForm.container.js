import React from "react";
import CreateContactForm from "./CreateContactForm.component";
import { Formik } from "formik";
import Modal from "../common/Modal";
import api from "../../utils/services";
import axios from "axios";
import useUsers from "../../state/users/hooks/useUsers";
import useModal from "../../state/modal/hooks/useModal";

const createUser = (values, setModal, getUsers) => {
  if (values.id) {
    axios.put(`${api}/${values.id}`, values).then(res => {
      setModal(false);
      getUsers();
    });
  } else {
    axios.post(api, values).then(res => {
      setModal(false);
      getUsers();
    });
  }
};

const CreateContactFormContainer = () => {
  const [users, getUsers] = useUsers();
  const [modal, setModal] = useModal();
  if (!modal.active) {
    return null;
  } else {
    return (
      <Formik
        onSubmit={values => ({
          id: values.id,
          name: values.name,
          email: values.email,
          phone: values.phone
        })}
        initialValues={modal.initialValues}
      >
        {({ values, dirty, handleChange }) => (
          <Modal
            title="Criar novo Contato"
            action={() => createUser(values, setModal, getUsers)}
            isDisabled={!dirty}
          >
            <CreateContactForm values={values} handleChange={handleChange} />
          </Modal>
        )}
      </Formik>
    );
  }
};

export default CreateContactFormContainer;
