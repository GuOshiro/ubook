import React from "react";
import Input from "../common/Input";
import Wrapper from "../common/Wrapper";

const CreateContactForm = ({ values, handleChange }) => (
  <Wrapper p="3">
    <form>
      <Input
        type="hidden"
        onChange={handleChange}
        name="id"
        value={values.id}
      />
      <Input
        label="Nome"
        onChange={handleChange}
        name="name"
        value={values.name}
      />
      <Input
        label="E-mail"
        onChange={handleChange}
        name="email"
        value={values.email}
      />
      <Input
        label="Telefone"
        onChange={handleChange}
        name="phone"
        value={values.phone}
      />
    </form>
  </Wrapper>
);

export default CreateContactForm;
