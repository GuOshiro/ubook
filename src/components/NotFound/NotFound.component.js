import React, { Fragment } from "react";
import PropType from "prop-types";
import { LabelText } from "../common/Typography";
import { ReactComponent as Icon } from "../../assets/svg/icon-book.svg";
import Wrapper from "../common/Wrapper";
import Button from "../common/Buttons";
import useModal from "../../state/modal/hooks/useModal";

const NotFound = ({ label, type, ...props }) => {
  const [modal, setModal] = useModal();

  return (
    <Fragment>
      <Wrapper
        {...props}
        display="flex"
        flexDirection="column"
        alignItems="center"
        pt="11"
      >
        <Icon />
        <LabelText mt="4" mb="4">
          Nenhum contato foi criado ainda
        </LabelText>
        <Button onClick={() => setModal(true)} label="+ Criar contato" />
      </Wrapper>
    </Fragment>
  );
};

NotFound.propType = {
  label: PropType.string,
  type: PropType.string
};

export default NotFound;
