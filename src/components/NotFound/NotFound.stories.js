import React from "react";
import NotFound from "./NotFound.component";

export default {
  title: "Not Found"
};

export const Default = () => <NotFound />;
