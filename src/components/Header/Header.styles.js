import styled from "styled-components";
import * as $D from "../../constants/dimensions";

export const Nav = styled.div`
  height: 50px;
  width: 100%;
  display: flex;
  align-items: center;
  padding: ${$D.PADDING};
  position: fixed;
  height: ${$D.HEADER_SIZE};
`;
