import React from "react";
import { Nav } from "./Header.styles";
import Wrapper from "../common/Wrapper";
import InputSearch from "../InputSearch";
import Button from "../common/Buttons";
import useModal from "../../state/modal/hooks/useModal";
import { ReactComponent as Icon } from "../../assets/svg/ubook.svg";

const Header = () => {
  const [modal, setModal] = useModal();
  return (
    <Nav>
      <Wrapper
        alignItems="center"
        display="flex"
        justifyContent="space-between"
      >
        <Icon />
        <Wrapper
          display="flex"
          alignContent="flex-end"
          justifyContent="flex-end"
        >
          <Button onClick={() => setModal(true)} mr='3' label="+ Criar contato" />
          <InputSearch onChange={e => console.log(e.target.value)} />
        </Wrapper>
      </Wrapper>
    </Nav>
  );
};

export default Header;
