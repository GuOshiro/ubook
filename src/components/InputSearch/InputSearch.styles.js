import styled from 'styled-components';
import { space, layout, color, position, flex } from 'styled-system'
import * as $C from '../../constants/colors'

export const Input = styled.input`
    background: transparent;
    border: none;
    outline: none;
    font-size: 14px;
    width: 278px;
    ::placeholder{
        color: ${$C.GRAY_DARK};
    }
`

export const Label = styled.label`
    align-items: center;
    background-color: ${$C.GRAY_LIGHT};
    border-radius: 4px;
    border-width: 0;
    box-shadow: none;
    display: flex;
    padding: 4px;
    justify-content: space-between;
    ${space} 
    ${layout} 
    ${color}
    ${position}
    ${flex}
`
