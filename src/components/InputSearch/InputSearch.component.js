import React from 'react'
import PropTypes from 'prop-types'
import { Input, Label } from './InputSearch.styles'
import { ReactComponent as SearchIcon } from '../../assets/svg/icon-search.svg'


const InputSearch = ({ placeholder, onChange, children, ...props }) => {
    return (
        <Label {...props}>
            <Input placeholder={placeholder} onChange={onChange} />
            <SearchIcon />
        </Label>
    )
}

InputSearch.defaultProps = {
    placeholder: 'Buscar...',
    onChange: () => { }
}

InputSearch.propTypes = {
    placeholder: PropTypes.string,
    onChange: PropTypes.func
}

export default InputSearch
