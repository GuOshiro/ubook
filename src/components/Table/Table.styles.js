import styled from "styled-components";
import { color } from "styled-system";
import * as $C from "../../constants/colors";

export const TableComponent = styled.table`
  width: 100%;
`;

export const ButtonIcon = styled.button`
  background: transparent;
  border: none;
  outline: none;
  cursor: pointer;
  svg {
    path {
      fill: ${$C.ORANGE};
    }
  }
`;

export const THead = styled.thead`
  padding: 16px 0;
  width: 100%;
  border-radius: 4px;
  border: solid 1px #e1e1e1;
  background-color: ${$C.WHITE};
  text-align: left;
  color: ${$C.GRAY_DARK};
  font-size: 13px;
  padding: 16px 8px;
  tr {
    height: 40px;
    th {
      vertical-align: middle;
    }
  }
`;

export const TBody = styled.tbody`
  width: 100%;
  height: 40px;
  border-radius: 4px;
  border: solid 1px #e1e1e1;
  background-color: ${$C.WHITE};
  text-align: left;
  color: ${$C.GRAY_DARK};
  font-size: 13px;
  padding: 16px 8px;
  tr {
    height: 40px;
    td {
      vertical-align: middle;
      :first-of-type {
        padding-left: 8px;
      }
    }
  }
`;

export const Avatar = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 100rem;
  text-align: center;
  padding-top: 6px;
  color: ${$C.WHITE};
  ${color};
`;
