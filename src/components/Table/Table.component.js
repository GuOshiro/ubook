import React from "react";
import { ReactComponent as DeleteIcon } from "../../assets/svg/ic-delete.svg";
import { ReactComponent as EditIcon } from "../../assets/svg/ic-edit.svg";
import {
  THead,
  TBody,
  TableComponent,
  Avatar,
  ButtonIcon
} from "./Table.styles";
import randomColor from "../../utils/randomColor";

const Table = ({ headers, body, editRow, deleteRow, callback }) => {
  const renderBody = body => {
    return body.list.map((item, index) => {
      const { id, email, name, phone } = item;
      return (
        <tr key={index}>
          <td>
            <Avatar backgroundColor={randomColor()}>{name[0]}</Avatar>
          </td>
          <td>{name}</td>
          <td>{email}</td>
          <td>{phone}</td>
          <td>
            <ButtonIcon onClick={() => editRow(item)}>
              <EditIcon />
            </ButtonIcon>
            <ButtonIcon onClick={() => deleteRow(id, callback)}>
              <DeleteIcon />
            </ButtonIcon>
          </td>
        </tr>
      );
    });
  };
  return (
    <TableComponent>
      <THead>
        <tr>
          {headers.map((header, index) => (
            <th key={index}>{header}</th>
          ))}
        </tr>
      </THead>
      <TBody>{renderBody(body)}</TBody>
    </TableComponent>
  );
};

export default Table;
