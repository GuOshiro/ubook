import React, { useState, Fragment } from "react";
import Table from "../Table/Table.component";
import axios from "axios";
import api from "../../utils/services";
import useUsers from "../../state/users/hooks/useUsers";
import useModal from "../../state/modal/hooks/useModal";

const deleteUser = (id, getUsers) => {
  axios.delete(`${api}/${id}`).then(res => getUsers());
};

const UsersList = ({ users }) => {
  const [user, getUsers] = useUsers();
  const [modal, setModal] = useModal();
  return (
    <Fragment>
      <Table
        headers={["", "Contatos", "E-mail", "Telefone", ""]}
        body={users}
        callback={getUsers}
        deleteRow={deleteUser}
        editRow={e => setModal(true, e)}
      />
    </Fragment>
  );
};

export default UsersList;
