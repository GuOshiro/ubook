import styled from "styled-components";
import { space, layout, color } from "styled-system";

export const InputComponent = styled.div`
  display: block;
  margin-top: 20px;
  margin-bottom: 16px;
  &.hidden{
    display: none;
  }
  ${space} 
  ${layout} 
  ${color}
`;

export const InputText = styled.input`
  margin-top: 4px
  width: 100%;
  height: 32px;
`;
