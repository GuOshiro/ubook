import React from "react";
import PropType from "prop-types";
import { InputComponent, InputText } from "./Input.styles";
import { SpanText } from "../Typography";

const Input = ({ type, label, onChange, name, value, ...props }) => {
  return (
    <InputComponent className={type} {...props}>
      <SpanText fontWeight="300">{label}</SpanText>
      <InputText onChange={onChange} name={name} value={value} />
    </InputComponent>
  );
};

Input.defaultProps = {
  children: "",
  type: ""
};

Input.proptype = {
  children: PropType.node,
  label: PropType.string,
  name: PropType.string,
  onChange: PropType.func,
  type: PropType.string,
  value: PropType.string
};

export default Input;
