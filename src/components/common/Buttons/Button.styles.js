import styled from "styled-components";
import * as $C from "../../../constants/colors";
import { space, layout, color } from "styled-system";

export const ButtonWrapper = styled.button`
    background: ${$C.GREEN_LIGHT}
    border-radius: 20px;
    border: solid 1px rgba(255, 255, 255, 0.16);
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16);
    color: ${$C.ORANGE};
    cursor: pointer;
    font-weight: 600;
    height: 40px;
    margin: 1px;
    transition: all .3s ease;
    width: 144px;
    &:hover {
        opacity: .7;
    }
    &.orange{
        background: ${$C.ORANGE}
        color: ${$C.WHITE}
    }
    &.text{
        background: none;
        border-radius: none;
        border: none;
        border: none;
        box-shadow: none;
        color: ${$C.ORANGE}
        font-size: 13px;
        height: auto:  
        outline: none;
        padding: 0px 14px;
        width: auto;
    }
    &:disabled {
        opacity: 0.6;
    }
    ${space};
    ${layout};
    ${color};
`;
