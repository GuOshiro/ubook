import React from "react";
import PropType from "prop-types";
import { ButtonWrapper } from "./Button.styles";

const Button = ({ isDisabled, label, type, ...props }) => (
  <ButtonWrapper disabled={isDisabled} {...props} className={type}>
    {label}
  </ButtonWrapper>
);

Button.defaultProp = {
  isDisabled: false,
  label: "Button Label",
  type: "text"
};

Button.propType = {
  isDisabled: PropType.bool,
  label: PropType.string,
  type: PropType.string
};

export default Button;
