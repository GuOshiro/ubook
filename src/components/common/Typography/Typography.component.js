import React from "react";
import theme from "../../../theme";
import DynamicType from "./DynamicType";

const { labelText, spanText } = theme.textStyles;

export const SpanText = props => (
  <DynamicType {...spanText} {...props}>
    {props.children}
  </DynamicType>
);

export const LabelText = props => (
  <DynamicType {...labelText} {...props}>
    {props.children}
  </DynamicType>
);
