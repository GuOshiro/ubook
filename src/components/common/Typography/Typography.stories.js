import React from "react";
import { storiesOf } from "@storybook/react";

import { LabelText, SpanText } from "./Typography.component";

storiesOf("Typography", module)
  .addDecorator(storyFn => <div style={{ padding: "16px" }}>{storyFn()}</div>)
  .add("default", () => (
    <>
      <div style={{ display: "flex" }}>
        <SpanText as="span" mb="2" mr="3">
          SpanText: Roboto - 14px / 18px
        </SpanText>
        <SpanText as="span" mb="2" fontWeight="bold">
          SpanText: Roboto - 14px / 18px BOLD
        </SpanText>
      </div>
      <div style={{ display: "flex" }}>
        <LabelText as="label" mb="2" mr="3">
          LabelText: Roboto - 12px / 14px
        </LabelText>
        <LabelText as="label" mb="2" fontWeight="bold">
          LabelText: Roboto - 12px / 14px BOLD
        </LabelText>
      </div>
    </>
  ));
