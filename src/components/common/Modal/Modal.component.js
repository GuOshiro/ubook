import React from "react";
import PropTypes from "prop-types";
import Wrapper from "../Wrapper";
import { ModalOverlay, ModalBody } from "./Modal.styles";
import Button from "../Buttons";
import { SpanText } from "../Typography";
import useModal from "../../../state/modal/hooks/useModal";

const Modal = ({ action, children, isDisabled, modalRef, title, ...props }) => {
  const [modal, setModal] = useModal();
  return (
    <ModalOverlay>
      <ModalBody {...props}>
        <Wrapper width="100%" p="3" borderBottom="1px solid #c0c3d2">
          <SpanText>{title}</SpanText>
        </Wrapper>
        {children}
        <Wrapper
          borderTop="1px solid #c0c3d2"
          display="flex"
          justifyContent="flex-end"
          p="3"
          width="100%"
        >
          <Button
            type="text"
            onClick={() => setModal(false)}
            label="Cancelar"
          />
          <Button
            type="orange"
            isDisabled={isDisabled}
            onClick={action}
            label="Salvar"
          />
        </Wrapper>
      </ModalBody>
    </ModalOverlay>
  );
};

Modal.propTypes = {
  action: PropTypes.func,
  children: PropTypes.node,
  isDisabled: PropTypes.bool,
  setVisibility: PropTypes.func,
  title: PropTypes.string,
  visibility: PropTypes.bool
};

export default Modal;
