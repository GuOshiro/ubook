import styled from "styled-components";
import * as $C from "../../../constants/colors";

export const ModalOverlay = styled.div`
  align-items: center;
  background: rgba(0, 0, 0, 0.3);
  bottom: 0;
  display: flex;
  justify-content: center;
  left: 0;
  position: fixed;
  right: 0;
  top: 0;
  transition: opacity 0.2s ease-out;
  vertical-align: middle;
  z-index: 10;
`;

export const ModalBody = styled.div`
  width: 432px;
  min-height: 342px;
  height: auto;
  border-radius: 16px;
  box-shadow: 0 16px 10px 0 rgba(0, 0, 0, 0.16);
  background-color: ${$C.WHITE};
`;
