import React from "react";
import { storiesOf } from "@storybook/react";
import ReactModal from "react-modal";

import Modal from "./Modal.container";
ReactModal.setAppElement("#root");

const mocks = [];

storiesOf("Modal", module)
  .addDecorator(storyFn => <div style={{ padding: "16px" }}>{storyFn()}</div>)
  .add("default", () => <Modal>{() => <p>Modal Component</p>}</Modal>);
