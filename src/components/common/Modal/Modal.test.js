import React from "react";
import ReactDOM from "react-dom";
import { MockedProvider } from "@apollo/react-testing";

import Modal from "./Modal.component";

const mocks = [];

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Modal />
    </MockedProvider>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
