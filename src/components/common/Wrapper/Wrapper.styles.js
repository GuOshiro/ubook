import styled from 'styled-components';
import { space, layout, color, background, flex, flexbox, border } from 'styled-system'
import * as $D from '../../../constants/dimensions'

export const Content = styled.div`
  width: 100%;
  .section{
    margin: 0 auto;
    max-width: ${$D.DESKTOP}
    padding: 70px 20px;
    ${space}
  }
  ${background}
  ${border}
  ${color}
  ${flex}
  ${flexbox}
  ${layout}
  ${space}
`

