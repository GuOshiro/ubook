export const GRAY_DARK = '#9198af';
export const GRAY_MEDIUM = '#f8f9fd';
export const GRAY_LIGHT = '#e4e7f4';
export const GREEN_LIGHT = '#dbff90';
export const ORANGE = '#fa7268';
export const ORANGE_LIGHT = '#fff3f2';
export const TRANSPARENT = 'transparent';
export const WHITE = '#fff';