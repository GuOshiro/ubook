export const DESKTOP = '1280px';
export const SMARTPHONE = '360px';
export const TABLET = '720px';
export const PADDING = '16px'
export const HEADER_SIZE = '64px';