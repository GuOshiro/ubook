import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";
import * as $C from "../constants/colors";

const BaseStyles = createGlobalStyle`
    ${reset}
    body { 
        background-color: ${$C.GRAY_MEDIUM};     
        margin: 0; 
        min-height: 100vh;
        min-width: 100vw; 
    };
    html { font-family: 'Roboto', sans-serif; }
    * { box-sizing: border-box; };
`;

export default BaseStyles;
