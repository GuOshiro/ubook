import React, { Suspense, lazy } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { ThemeProvider } from "styled-components";

import { INITIAL_STATE as LOADER_INITIAL_STATE } from "../state/loader/reducers";
import { INITIAL_STATE as MODAL_INITIAL_STATE } from "../state/modal/reducers";
import { INITIAL_STATE as USERS_INITIAL_STATE } from "../state/users/reducers";

import { StateProvider } from "../state";
import reducers from "../state/reducers";
import BaseStyles from "./base-styles";
import theme from "../theme";

import Header from "../components/Header";
import Loader from "../components/Loader";
import Wrapper from "../components/common/Wrapper";
import CreateContactForm from "../components/CreateContactForm";

// Routes
const Users = lazy(() => import("../views/users"));

const Root = () => {
  const initialState = {
    loader: LOADER_INITIAL_STATE,
    modal: MODAL_INITIAL_STATE,
    users: USERS_INITIAL_STATE
  };
  return (
    <StateProvider initialState={initialState} reducer={reducers}>
      <Suspense fallback={<Loader show={true} />}>
        <BaseStyles />
        <Router>
          <ThemeProvider theme={theme}>
            <Header />
            <Wrapper p="3" pt="8">
              <Switch>
                <Redirect exact from="/" to="/home" component={Users} />
                <Route exact path="/home" component={Users} />
              </Switch>
            </Wrapper>
            <CreateContactForm />
          </ThemeProvider>
        </Router>
      </Suspense>
    </StateProvider>
  );
};

export default Root;
