import React, { useEffect } from "react";
import NotFound from "../../components/NotFound";
import UsersList from "../../components/UsersList";
import useUsers from "../../state/users/hooks/useUsers";

const Users = () => {
  const [users, getUsers] = useUsers();
  useEffect(() => {
    if (users.list === null) {
      getUsers();
    }
  }, [users.list]);
  if (users.list === null || users.list.length === 0) {
    return <NotFound />;
  } else {
    return <UsersList users={users} />;
  }
};

export default Users;
