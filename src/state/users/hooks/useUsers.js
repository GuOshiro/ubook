import { useStateValue } from '../../index'
import { loadUsers } from '../queries'
import { listUsers } from '../actions'
import useLoader from '../../loader/hooks/useLoader'

const useUsers = () => {
  const [{ users }, dispatch] = useStateValue()
  const [load, setLoad] = useLoader();

  const request = async () => {
    setLoad(true)
    const response = await loadUsers();
    if (response) {
      dispatch(listUsers(response))
    } else {
      const err = []
      dispatch(listUsers(err))
    }
    setLoad(false)
  }

  return [users, request]
}

export default useUsers
