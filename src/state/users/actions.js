export const LIST_USERS = 'pages/LIST_USERS';

export const listUsers = payload => ({
  type: LIST_USERS,
  payload: payload
});
