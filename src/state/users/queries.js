import axios from "axios";
import api from "../../utils/services";

export const loadUsers = () => {
  return axios.get(api)
    .then(res => res.data)
    .catch(err => err.response);
};
