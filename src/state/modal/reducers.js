import { MODAL } from "./actions";

export const INITIAL_STATE = {
  active: false,
  initialValues: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MODAL:
      return {
        ...state,
        active: action.payload[0],
        initialValues: action.payload[1]
      };
    default:
      return state;
  }
};
