import { useStateValue } from "../../index";
import { opeModal } from "../actions";

const useModal = () => {
  const [{ modal }, dispatch] = useStateValue();

  const request = async (active, intialValues) => {
    dispatch(opeModal([active, intialValues]));
  };

  return [modal, request];
};

export default useModal;
