import usersReducer from "./users/reducers";
import loaderReducer from "./loader/reducers";
import modalReducer from "./modal/reducers";

export default ({ users, loader, modal }, action) => ({
  users: usersReducer(users, action),
  modal: modalReducer(modal, action),
  loader: loaderReducer(loader, action)
});
