# Build


Run: ```npm install```

Storybook: ```npm run storybook```

Test: ```npm run test```

After run: ```npm start```



Check below our stacks

- React
- Context
- Hooks (useReducer, useContext)
- State Global using Context + Hooks
- Custom Hooks (Like redux Thunks)
- Axios
- Private-Route
- Styled-component with GlobalStyle
- Storybook
- Jest


